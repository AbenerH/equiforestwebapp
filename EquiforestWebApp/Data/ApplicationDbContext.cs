﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using EquiforestWebApp.Models;

namespace EquiforestWebApp.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole,string>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<EquiforestWebApp.Models.Trozas> Trozas { get; set; }
        public DbSet<EquiforestWebApp.Models.Viajes> Viajes { get; set; }
        public DbSet<EquiforestWebApp.Models.Rodales> Rodales { get; set; }
        public DbSet<EquiforestWebApp.Models.Facturas> Facturas { get; set; }
        public DbSet<EquiforestWebApp.Models.Contenedores> Contenedores { get; set; }
        public DbSet<EquiforestWebApp.Models.DetalleContenedores> DetalleContenedores { get; set; }
        public DbSet<EquiforestWebApp.Models.PackingList> PackingList { get; set; }
        public DbSet<EquiforestWebApp.Models.Precios> Precios { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
            builder.Entity<Viajes>()
                .HasKey(k => k.Id);

            builder.Entity<Viajes>()
            .HasAlternateKey(i => i.ViajeId);

            builder.Entity<Trozas>()
            .HasOne(p => p.Viaje)
            .WithMany(b => b.Trozas)
            .HasForeignKey(p => p.ViajeId)
            .HasConstraintName("AlternateKey_ViajeId")
            .HasPrincipalKey(k => k.ViajeId);

            builder.Entity<PackingList>()
            .HasKey(k => k.Id);

            builder.Entity<PackingList>()
            .HasAlternateKey(i => i.PackingListId);

            builder.Entity<DetalleContenedores>()
            .HasOne(p => p.PackingList)
            .WithMany(b => b.DetalleContenedor)
            .HasForeignKey(p => p.PackingListId)
            .HasConstraintName("AlternateKey_Contenedor")
            .HasPrincipalKey(k => k.PackingListId);

            builder.Entity<Facturas>()
            .HasKey(k => k.Id);

            builder.Entity<Facturas>()
            .HasAlternateKey(i => i.FacturaId);

            builder.Entity<Contenedores>()
            .HasKey(k => k.Id);

            builder.Entity<Contenedores>()
            .HasAlternateKey(i => i.ContenedorId);

            builder.Entity<Contenedores>()
            .HasOne(p => p.Factura)
            .WithMany(c => c.Contenedores)
            .HasForeignKey(p => p.FacturaId)
            .HasConstraintName("AlternateKey_ContenedoresFactura")
            .HasPrincipalKey(k => k.FacturaId);

        }
    }
}
