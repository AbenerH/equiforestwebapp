﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EquiforestWebApp.Models
{
    public interface IDbInitializer
    {
        void Initialize();
    }
}
