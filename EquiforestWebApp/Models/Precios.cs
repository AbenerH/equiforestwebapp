﻿using System.ComponentModel.DataAnnotations;


namespace EquiforestWebApp.Models
{
    public class Precios
    {
        public int Id { get; set; }
        [Required, MaxLength(10)]
        public string FacturaId { get; set; }
        public string Clase { get; set; }
        public string Estrato { get; set; }
        public float PrecioA { get; set; }
        public float PrecioB { get; set; }
        public float PrecioC { get; set; }
    }
}
