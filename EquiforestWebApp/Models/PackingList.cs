﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EquiforestWebApp.Models
{
    public class PackingList
    {
        public int Id { get; set; }
        public DateTime Fecha { get; set; }
        [Required]
        public string Ubicacion { get; set; }
        public int Consecutivo { get; set; }
        public string Clase { get; set; }
        [Column(TypeName = "varchar(30)")]
        public string PackingListId { get; set; }
        public string Estado { get; set; }
        public float DescuentoCrc { get; set; }
        public float DescuentoLong { get; set; }
        [Column(TypeName = "varchar(20)")]
        public string ContenedorId { get; set; }
        public List<DetalleContenedores> DetalleContenedor { get; set; }
    }
}
