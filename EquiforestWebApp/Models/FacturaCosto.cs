﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EquiforestWebApp.Models
{
    public class FacturaCosto
    {
        public int Id { get; set; }
        public float MinValue { get; set; }
        public float MaxValue { get; set; }
        public string Name { get; set; }
        public float CantidadA { get; set; }
        public float CantidadB { get; set; }
        public float CantidadC { get; set; }
        public float VolumenA { get; set; }
        public float VolumenB { get; set; }
        public float VolumenC { get; set; }
        public float PrecioA { get; set; }
        public float PrecioB { get; set; }
        public float PrecioC { get; set; }
        public float CostoA { get; set; }
        public float CostoB { get; set; }
        public float CostoC { get; set; }
    }
}
