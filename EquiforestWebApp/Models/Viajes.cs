﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EquiforestWebApp.Models
{
    public class Viajes
    {
        public int Id { get; set; }
        [Column(TypeName = "varchar(30)")]
        public string ViajeId { get; set; }
        [Required]
        public string Nucleo { get; set; }
        [Required]
        public string Finca { get; set; }
        [Required]
        public string Rodal { get; set; }
        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Fecha de Ingreso")]
        public DateTime FechaIngreso { get; set; }
        [Required]
        public string Ubicacion { get; set; }
        [Required]
        public int? N_Viaje { get; set; }
        [Display(Name = "Trozas C")]
        public int? NoTrozasC { get; set; }
        public List<Trozas> Trozas { get; set; }
        [NotMapped]
        public List<SelectListItem> Nucleos { get; set; }
        [NotMapped]
        public List<SelectListItem> Fincas { get; set; }
        [NotMapped]
        public List<SelectListItem> Rodales { get; set; }
    }
}
