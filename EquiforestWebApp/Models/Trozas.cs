﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EquiforestWebApp.Models
{
    public class Trozas
    {
        public int Id { get; set; }
        [Required]
        public string Clasificacion { get; set; }
        [Required]
        public float? Circunferencia { get; set; }
        [Required]
        public float? Largo { get; set; }
        [Column(TypeName = "varchar(30)")]
        public string ViajeId { get; set; }
        [ForeignKey("ViajeId")]
        public Viajes Viaje { get; set; }
    }
}
