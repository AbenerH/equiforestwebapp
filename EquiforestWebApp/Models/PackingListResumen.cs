﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EquiforestWebApp.Models
{
    public class PackingListResumen
    {
        public string Cliente { get; set; }
        public DateTime Fecha { get; set; }
        public string FacturaId { get; set; }
        public string Cabezal { get; set; }
        public string Especie { get; set; }
        public string Calidad { get; set; }
        public string ContenedorId { get; set; }
        public string Finca { get; set; }
        public string Chofer { get; set; }
        public string Sello { get; set; }
        public string Grupo { get; set; }
        public string PackingListId { get; set; }
        public int Cantidad { get; set; }
        public float Diametro { get; set; }
        public float Largo { get; set; }
        public float Volumen { get; set; }
        public float VolumenH { get; set; }
        public float Precio { get; set; }
        public double Costo { get; set; }
    }
}
