﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EquiforestWebApp.Models
{
    public class ResumenComercial
    {
        public string Clasificacion { get; set; }
        public float Circunferencia { get; set; }
        public float Longitud { get; set; }
        public float? VolumenS { get; set; }
        public float? VolumenH { get; set; }
        public string Estrato { get; set; }
        public float Diametro { get; set; }
        public int Cantidad { get; set; }
    }
}
