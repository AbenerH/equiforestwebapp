﻿using EquiforestWebApp.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EquiforestWebApp.Models
{
    public class DbInitializer : IDbInitializer
    {
        private readonly IServiceProvider _serviceProvider;

        public DbInitializer(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        //This example just creates an Administrator role and one Admin users
        public async void Initialize()
        {
            using (var serviceScope = _serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                //create database schema if none exists
                var context = serviceScope.ServiceProvider.GetService<ApplicationDbContext>();
                context.Database.EnsureCreated();

                //If there is already an Administrator role, abort
                var _roleManager = serviceScope.ServiceProvider.GetService<RoleManager<ApplicationRole>>();
                if (!(await _roleManager.RoleExistsAsync("Administrator")))
                {
                    //Create the Administartor Role
                    await _roleManager.CreateAsync(new ApplicationRole("Administrator"));
                }

                if (!(await _roleManager.RoleExistsAsync("User")))
                {
                    //Create the Administartor Role
                    await _roleManager.CreateAsync(new ApplicationRole("User"));
                }

                if (!(await _roleManager.RoleExistsAsync("HarvestManager")))
                {
                    //Create the Administartor Role
                    await _roleManager.CreateAsync(new ApplicationRole("HarvestManager"));
                }

                if (!(await _roleManager.RoleExistsAsync("SalesManager")))
                {
                    //Create the Administartor Role
                    await _roleManager.CreateAsync(new ApplicationRole("SalesManager"));
                }

                if (!(await _roleManager.RoleExistsAsync("HarvestOperator")))
                {
                    //Create the Administartor Role
                    await _roleManager.CreateAsync(new ApplicationRole("HarvestOperator"));
                }

                if (!(await _roleManager.RoleExistsAsync("SalesOperator")))
                {
                    //Create the Administartor Role
                    await _roleManager.CreateAsync(new ApplicationRole("SalesOperator"));
                }

                //Create the default Admin account and apply the Administrator role
                var _userManager = serviceScope.ServiceProvider.GetService<UserManager<ApplicationUser>>();
                string user = "jtonyhdz@gmail.com";
                //await _userManager.AddToRoleAsync(await _userManager.FindByNameAsync(user), "Administrator");
                string password = "qKUaumdt.98";
                var success = await _userManager.CreateAsync(new ApplicationUser { UserName = user, Email = user, EmailConfirmed = true }, password);
                if (success.Succeeded)
                {
                    await _userManager.AddToRoleAsync(await _userManager.FindByNameAsync(user), "Administrator");
                }
            }
        }

        public async void SetRole(string user, string role)
        {
            using (var serviceScope = _serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var _userManager = serviceScope.ServiceProvider.GetService<UserManager<ApplicationUser>>();
                await _userManager.AddToRoleAsync(await _userManager.FindByNameAsync(user), role);
            }
        }
    }
}
