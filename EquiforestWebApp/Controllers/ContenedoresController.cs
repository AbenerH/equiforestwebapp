﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EquiforestWebApp.Data;
using EquiforestWebApp.Models;
using OfficeOpenXml;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.JsonPatch;

namespace EquiforestWebApp.Controllers
{
    public class ContenedoresController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly FacturasController _facturasController;

        public ContenedoresController(ApplicationDbContext context)
        {
            _context = context;
            _facturasController = new FacturasController(context);
        }

        // GET: Contenedores
        public async Task<IActionResult> Index(int? pageNumber, DateTime fecini, DateTime fecfin)
        {
            int pageSize = 10;
            var _fecini = fecini.Year < 1900 ? new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1) : fecini.Date;
            var _fecfin = fecfin.Year < 1900 ? new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month)) : fecfin.Date;
            ViewData["FecIni"] = _fecini.ToString("yyyy-MM-dd");
            ViewData["FecFin"] = _fecfin.ToString("yyyy-MM-dd");
            //return View(await _context.Facturas.ToListAsync());
            return View(await PaginatedList<Contenedores>.CreateAsync(_context.Contenedores.Include(t => t.Factura).Where(f => f.FechaSalida >= _fecini && f.FechaSalida <= _fecfin).AsNoTracking(), pageNumber ?? 1, pageSize));

            //var applicationDbContext = _context.Contenedores.Include(c => c.Factura);
            //return View(await applicationDbContext.ToListAsync());
        }

        // GET: Contenedores/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var contenedores = await _context.Contenedores
                .Include(c => c.Factura)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (contenedores == null)
            {
                return NotFound();
            }

            return View(contenedores);
        }

        // GET: Contenedores/Create
        public IActionResult Create()
        {
            ViewData["FacturaId"] = new SelectList(_context.Facturas, "FacturaId", "FacturaId");
            return View();
        }

        // POST: Contenedores/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,FechaSalida,ContenedorId,Cabezal,Sello,Chofer,DescuentoCrc,DescuentoLong,MaxCargoWgt,FacturaId,PackingListId")] Contenedores contenedores)
        {
            if (ModelState.IsValid)
            {
                _context.Add(contenedores);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["FacturaId"] = new SelectList(_context.Facturas, "FacturaId", "FacturaId", contenedores.FacturaId);
            return View(contenedores);
        }

        // GET: Contenedores/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var contenedores = await _context.Contenedores.SingleOrDefaultAsync(m => m.Id == id);
            if (contenedores == null)
            {
                return NotFound();
            }
            ViewData["FacturaId"] = new SelectList(_context.Facturas, "FacturaId", "FacturaId", contenedores.FacturaId);
            return View(contenedores);
        }

        // POST: Contenedores/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,FechaSalida,ContenedorId,Cabezal,Sello,Chofer,DescuentoCrc,DescuentoLong,MaxCargoWgt,FacturaId,PackingListId")] Contenedores contenedores)
        {
            if (id != contenedores.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(contenedores);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ContenedoresExists(contenedores.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["FacturaId"] = new SelectList(_context.Facturas, "FacturaId", "FacturaId", contenedores.FacturaId);
            return View(contenedores);
        }

        // GET: Contenedores/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var contenedores = await _context.Contenedores
                .Include(c => c.Factura)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (contenedores == null)
            {
                return NotFound();
            }

            return View(contenedores);
        }

        // POST: Contenedores/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var contenedores = await _context.Contenedores.SingleOrDefaultAsync(m => m.Id == id);
            _context.Contenedores.Remove(contenedores);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ContenedoresExists(int id)
        {
            return _context.Contenedores.Any(e => e.Id == id);
        }


        public async Task<IActionResult> ImportFromExcel(IFormFile theExcel)
        {
            if (theExcel == null)
            {
                return BadRequest();
            }

            ExcelPackage excel;
            using (var memoryStream = new MemoryStream())
            {
                await theExcel.CopyToAsync(memoryStream);
                excel = new ExcelPackage(memoryStream);
            }
            var workSheet = excel.Workbook.Worksheets[2];

            var start = workSheet.Dimension.Start;
            var end = workSheet.Dimension.End;
            for (int row = start.Row + 1; row <= end.Row; row++)
            {
                if (workSheet.Cells[row, 1].Value != null)
                {
                    Contenedores contenedor = new Contenedores
                    {
                        ContenedorId = workSheet.Cells[row, 1].Text,
                        Sello = workSheet.Cells[row, 2].Text,
                        Cabezal = workSheet.Cells[row, 3].Text,
                        Chofer = workSheet.Cells[row, 4].Text,
                        FechaSalida = (DateTime) workSheet.Cells[row, 6].Value,
                        MaxCargoWgt = Convert.ToSingle(workSheet.Cells[row, 7].Value),
                        PackingListId = workSheet.Cells[row, 8].Text,
                    };

                    var result = _context.PackingList.FirstOrDefault(m => m.PackingListId == contenedor.PackingListId);

                    if(result!= null)
                    {
                        await updatePackingContainer(contenedor.ContenedorId, result);
                    }
                    _context.Contenedores.Add(contenedor);
                }
            }

            _context.SaveChanges();
            return RedirectToAction(nameof(Index));
        }

        private async Task<IActionResult> updatePackingContainer(string contenedorId, Models.PackingList result)
        {
            JsonPatchDocument<PackingList> patchDoc = new JsonPatchDocument<PackingList>();
            patchDoc.Replace(e => e.ContenedorId, contenedorId);

            return await _facturasController.PatchPackingListAsync(result.PackingListId, patchDoc);

        }
    }
}
