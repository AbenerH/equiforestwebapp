﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EquiforestWebApp.Data;
using EquiforestWebApp.Models;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System.IO;
using Microsoft.AspNetCore.JsonPatch;

namespace EquiforestWebApp.Controllers
{
    public class FacturasController : Controller
    {
        private readonly ApplicationDbContext _context;
 
        public FacturasController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Facturas
        public async Task<IActionResult> Index(int? pageNumber, DateTime fecini, DateTime fecfin)
        {
            int pageSize = 10;
            var _fecini = fecini.Year < 1900 ? new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1) : fecini.Date;
            var _fecfin = fecfin.Year < 1900 ? new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month)) : fecfin.Date;
            ViewData["FecIni"] = _fecini.ToString("yyyy-MM-dd");
            ViewData["FecFin"] = _fecfin.ToString("yyyy-MM-dd");

            //return View(await _context.Facturas.ToListAsync());
            return View(await PaginatedList<Facturas>.CreateAsync(_context.Facturas.Include(t => t.Contenedores).Where(f => f.Fecha >= _fecini && f.Fecha <= _fecfin).AsNoTracking(), pageNumber ?? 1, pageSize));
        }

        public async Task<IActionResult> PackingList(string Facturano, string packinglistid, DateTime fecini, DateTime fecfin, int origen)
        {
            var _fecini = fecini.Year < 1900 ? new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1) : fecini.Date;
            var _fecfin = fecfin.Year < 1900 ? new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month)) : fecfin.Date;
            ViewData["FecIni"] = _fecini.ToString("yyyy-MM-dd");
            ViewData["FecFin"] = _fecfin.ToString("yyyy-MM-dd");

            List<Estratos> _estratos = new List<Estratos>();
            _estratos.Add(new Estratos { Id = 0, MinValue = 12, MaxValue = 13, Name = "12"});
            _estratos.Add(new Estratos { Id = 1, MinValue = 13, MaxValue = 14, Name = "13"});
            _estratos.Add(new Estratos { Id = 2, MinValue = 14, MaxValue = 15, Name = "14"});
            _estratos.Add(new Estratos { Id = 3, MinValue = 15, MaxValue = 16, Name = "15"});
            _estratos.Add(new Estratos { Id = 4, MinValue = 16, MaxValue = 17, Name = "16"});
            _estratos.Add(new Estratos { Id = 5, MinValue = 17, MaxValue = 18, Name = "17"});
            _estratos.Add(new Estratos { Id = 6, MinValue = 18, MaxValue = 19, Name = "18"});
            _estratos.Add(new Estratos { Id = 7, MinValue = 19, MaxValue = 20, Name = "19"});
            _estratos.Add(new Estratos { Id = 8, MinValue = 20, MaxValue = 21, Name = "20"});
            _estratos.Add(new Estratos { Id = 9, MinValue = 21, MaxValue = 22, Name = "21"});
            _estratos.Add(new Estratos { Id = 10, MinValue = 22, MaxValue = 23, Name = "22"});
            _estratos.Add(new Estratos { Id = 11, MinValue = 23, MaxValue = 24, Name = "23"});
            _estratos.Add(new Estratos { Id = 12, MinValue = 24, MaxValue = 25, Name = "24"});
            _estratos.Add(new Estratos { Id = 13, MinValue = 25, MaxValue = 99999, Name = "25",});

            List<Estratos> _estratoPrecios = new List<Estratos>();
            _estratoPrecios.Add(new Estratos { Id = 0, MinValue = 40, MaxValue = 45, Name = "40-44"});
            _estratoPrecios.Add(new Estratos { Id = 1, MinValue = 45, MaxValue = 50, Name = "45-49"});
            _estratoPrecios.Add(new Estratos { Id = 2, MinValue = 50, MaxValue = 55, Name = "50-54"});
            _estratoPrecios.Add(new Estratos { Id = 3, MinValue = 55, MaxValue = 60, Name = "55-59"});
            _estratoPrecios.Add(new Estratos { Id = 4, MinValue = 60, MaxValue = 65, Name = "60-64"});
            _estratoPrecios.Add(new Estratos { Id = 5, MinValue = 65, MaxValue = 70, Name = "65-69"});
            _estratoPrecios.Add(new Estratos { Id = 6, MinValue = 70, MaxValue = 75, Name = "70-74"});
            _estratoPrecios.Add(new Estratos { Id = 7, MinValue = 75, MaxValue = 80, Name = "75-79"});
            _estratoPrecios.Add(new Estratos { Id = 8, MinValue = 80, MaxValue = 85, Name = "80-84"});
            _estratoPrecios.Add(new Estratos { Id = 9, MinValue = 85, MaxValue = 90, Name = "85-89"});
            _estratoPrecios.Add(new Estratos { Id = 10, MinValue = 90, MaxValue = 99999, Name = "90->>"});

            ViewData["Estratos"] = _estratos;
            ViewData["EstratosPrecios"] = _estratoPrecios;
            ViewData["ShowFilter"] = true;
            ViewData["Precios"] = null;
            List<PackingListResumen> model = null;

            switch (origen)
            {
                //Comes from Factura
                case 1:
                    ViewData["ShowFilter"] = false;
                    var result1 = 
                        from s in _context.DetalleContenedores
                                 join v in _context.PackingList on s.PackingListId equals v.PackingListId
                                 join c in _context.Contenedores on v.PackingListId equals c.PackingListId
                                 join f in _context.Facturas on c.FacturaId equals f.FacturaId
                                 where f.FacturaId == Facturano
                                 select new PackingListResumen
                                 {
                                     Cliente = f.Cliente,
                                     Fecha = f.Fecha,
                                     FacturaId = f.FacturaId,
                                     Cabezal = c.Cabezal,
                                     Especie = f.Especie,
                                     Calidad = v.Clase,
                                     ContenedorId = c.ContenedorId,
                                     Finca = f.Finca,
                                     Chofer = c.Chofer,
                                     Sello = c.Sello,
                                     Grupo = "Trozas con corteza",
                                     PackingListId = v.PackingListId,
                                     Diametro = Convert.ToSingle(s.Circunferencia / Math.PI),
                                     Largo = s.Longitud,
                                     Volumen = MathF.Round((float)(((s.Circunferencia * s.Circunferencia) / (4 * Math.PI)) * s.Longitud / 10000), 2),
                                     VolumenH = Convert.ToSingle((Math.Pow(((s.Circunferencia / 100.0f) - v.DescuentoCrc), 2) * (s.Longitud - v.DescuentoLong) / 16)),
                                 };
                    
                    model = await result1.OrderBy(o => o.Fecha).ThenBy(o => o.FacturaId).AsNoTracking().ToListAsync();
                    break;
                
                //Comes from PackingList - Global
                case 2:
                    var result2 = from s in _context.DetalleContenedores
                                 join v in _context.PackingList on s.PackingListId equals v.PackingListId
                                 where v.Fecha >= _fecini && v.Fecha <= _fecfin && v.ContenedorId == null
                                 select new PackingListResumen
                                 {
                                     Cliente = "",
                                     Fecha = v.Fecha,
                                     FacturaId = "",
                                     Cabezal = "",
                                     Especie = "Teca",
                                     Calidad = v.Clase,
                                     ContenedorId = "",
                                     Finca = v.Ubicacion,
                                     Chofer = "",
                                     Sello = "",
                                     Grupo = "Trozas con corteza",
                                     PackingListId = v.PackingListId,
                                     Diametro = Convert.ToSingle(s.Circunferencia / Math.PI),
                                     Largo = s.Longitud,
                                     Volumen = MathF.Round((float)(((s.Circunferencia * s.Circunferencia) / (4 * Math.PI)) * s.Longitud / 10000), 2)
                                 };
                    model = await result2.OrderBy(o => o.Fecha).ThenBy(o => o.FacturaId).AsNoTracking().ToListAsync();
                    break;
                
                //Comes from PackingList - Print
                case 3:
                    ViewData["ShowFilter"] = false;
                    var result3 = from s in _context.DetalleContenedores
                                  join v in _context.PackingList on s.PackingListId equals v.PackingListId
                                  where v.PackingListId == packinglistid
                                  select new PackingListResumen
                                  {
                                      Cliente = "",
                                      Fecha = v.Fecha,
                                      FacturaId = "",
                                      Cabezal = "",
                                      Especie = "Teca",
                                      Calidad = v.Clase,
                                      ContenedorId = "",
                                      Finca = v.Ubicacion,
                                      Chofer = "",
                                      Sello = "",
                                      Grupo = "Trozas con corteza",
                                      PackingListId = v.PackingListId,
                                      Diametro = Convert.ToSingle(s.Circunferencia / Math.PI),
                                      Largo = s.Longitud,
                                      Volumen = MathF.Round((float)(((s.Circunferencia * s.Circunferencia) / (4 * Math.PI)) * s.Longitud / 10000), 2)
                                  };
                    model = await result3.OrderBy(o => o.Fecha).ThenBy(o => o.FacturaId).AsNoTracking().ToListAsync();
                    break;
            }

            /*if (Facturano == null )
            {
                var result = from s in _context.DetalleContenedores
                             join v in _context.PackingList on s.PackingListId equals v.PackingListId
                             where v.Fecha >= _fecini && v.Fecha <= _fecfin && v.ContenedorId == null
                             select new PackingListResumen
                             {
                                 Cliente = "",
                                 Fecha = v.Fecha,
                                 FacturaId = "",
                                 Cabezal = "",
                                 Especie = "",
                                 Calidad = v.Clase,
                                 ContenedorId = "",
                                 Finca = v.Ubicacion,
                                 Chofer = "",
                                 Sello = "",
                                 Grupo = "Trozas con corteza",
                                 PackingListId = v.PackingListId,
                                 Diametro = Convert.ToSingle(s.Circunferencia / Math.PI),
                                 Largo = s.Longitud,
                                 Volumen = MathF.Round((float)(((s.Circunferencia * s.Circunferencia) / (4 * Math.PI)) * s.Longitud / 10000), 2)
                             };
                model = await result.OrderBy(o => o.Fecha).ThenBy(o => o.FacturaId).AsNoTracking().ToListAsync();
            }
            else
            {
                ViewData["ShowFilter"] = false;
                var result = from s in _context.DetalleContenedores
                             join v in _context.PackingList on s.PackingListId equals v.PackingListId
                             join c in _context.Contenedores on v.PackingListId equals c.PackingListId
                             join f in _context.Facturas on c.FacturaId equals f.FacturaId
                             where f.FacturaId == Facturano
                             select new PackingListResumen
                             {
                                 Cliente = f.Cliente,
                                 Fecha = f.Fecha,
                                 FacturaId = f.FacturaId,
                                 Cabezal = c.Cabezal,
                                 Especie = f.Especie,
                                 Calidad = v.Clase,
                                 ContenedorId = c.ContenedorId,
                                 Finca = f.Finca,
                                 Chofer = c.Chofer,
                                 Sello = c.Sello,
                                 Grupo = "Trozas con corteza",
                                 PackingListId = v.PackingListId,
                                 Diametro = Convert.ToSingle(s.Circunferencia / Math.PI),
                                 Largo = s.Longitud,
                                 Volumen = MathF.Round((float)(((s.Circunferencia * s.Circunferencia) / (4 * Math.PI)) * s.Longitud / 10000), 2)
                             };
                model = await result.OrderBy(o => o.Fecha).ThenBy(o => o.FacturaId).AsNoTracking().ToListAsync();
            }*/
            //Get the prices table

            var prices = _context.Precios.Where(f => f.FacturaId == Facturano).AsNoTracking().ToList();

            var packingList = model.OrderBy(o => o.PackingListId).GroupBy(g => g.PackingListId);

            if(origen==1)
            {
                var precios = _context.Precios.Where(f => f.FacturaId == Facturano).AsNoTracking().ToList();
                var precios2 = from s in precios
                               join v in _estratoPrecios on s.Estrato equals v.Name
                               select new FacturaCosto
                               {
                                   Id = v.Id,
                                   MinValue = v.MinValue,
                                   MaxValue = v.MaxValue,
                                   Name = v.Name,
                                   CantidadA = 0.0f,
                                   CantidadB = 0.0f,
                                   CantidadC = 0.0f,
                                   VolumenA = 0.0f,
                                   VolumenB = 0.0f,
                                   VolumenC = 0.0f,
                                   PrecioA = s.PrecioA,
                                   PrecioB = s.PrecioB,
                                   PrecioC = s.PrecioC,
                                   CostoA = 0.0f,
                                   CostoB = 0.0f,
                                   CostoC = 0.0f
                               };

                foreach (var estrato in precios2)
                {
                    estrato.CantidadA = model.Where(w => w.Diametro >= estrato.MinValue && w.Diametro <= estrato.MaxValue && w.Calidad == "A").Count();
                    estrato.CantidadB = model.Where(w => w.Diametro >= estrato.MinValue && w.Diametro <= estrato.MaxValue && w.Calidad == "B").Count();
                    estrato.CantidadC = model.Where(w => w.Diametro >= estrato.MinValue && w.Diametro <= estrato.MaxValue && w.Calidad == "C").Count();
                    estrato.VolumenA = model.Where(w => w.Diametro >= estrato.MinValue && w.Diametro <= estrato.MaxValue && w.Calidad == "A").Sum(s => s.VolumenH);
                    estrato.VolumenB = model.Where(w => w.Diametro >= estrato.MinValue && w.Diametro <= estrato.MaxValue && w.Calidad == "B").Sum(s => s.VolumenH);
                    estrato.VolumenC = model.Where(w => w.Diametro >= estrato.MinValue && w.Diametro <= estrato.MaxValue && w.Calidad == "C").Sum(s => s.VolumenH);
                    estrato.CostoA = estrato.VolumenA * estrato.PrecioA;
                    estrato.CostoB = estrato.VolumenB * estrato.PrecioB;
                    estrato.CostoC = estrato.VolumenC * estrato.PrecioC;
                }
                //_estratos.Add(new Estratos { Id = 1, MinValue = 45, MaxValue = 50, Name = "45-49" });
                ViewData["Precios"] = precios2;
            }
                //_context.PackingList.Where(f => f.FacturaId == Facturano).AsNoTracking().ToList();
            ViewData["PackingList"] = packingList;
            ViewData["Factura"] = Facturano;
            return View(model);
        }

        // GET: Facturas/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var facturas = await _context.Facturas
                .SingleOrDefaultAsync(m => m.Id == id);
            if (facturas == null)
            {
                return NotFound();
            }

            return View(facturas);
        }

        // GET: Facturas/Create
        public IActionResult Create()
        {
            ViewData["Fincas"] = GetFincas();
            ViewData["Incoterms"] = GetIncoterms();
            ViewData["IncotermSelected"] = "";
            return View();
        }

        // POST: Facturas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,FacturaId,Fecha,Cliente,Especie,Finca,Incoterm,Destino,BillOfLading")] Facturas facturas)
        {
            if (ModelState.IsValid)
            {
                _context.Add(facturas);
                //Create the prices 
                List<Precios> precios = new List<Precios>()
                {
                    new Precios {FacturaId=facturas.FacturaId, Estrato = "40-44"},
                    new Precios {FacturaId=facturas.FacturaId, Estrato = "45-49"},
                    new Precios {FacturaId=facturas.FacturaId, Estrato = "50-54"},
                    new Precios {FacturaId=facturas.FacturaId, Estrato = "55-59"},
                    new Precios {FacturaId=facturas.FacturaId, Estrato = "60-64"},
                    new Precios {FacturaId=facturas.FacturaId, Estrato = "65-69"},
                    new Precios {FacturaId=facturas.FacturaId, Estrato = "70-74"},
                    new Precios {FacturaId=facturas.FacturaId, Estrato = "75-79"},
                    new Precios {FacturaId=facturas.FacturaId, Estrato = "80-84"},
                    new Precios {FacturaId=facturas.FacturaId, Estrato = "85-89"},
                    new Precios {FacturaId=facturas.FacturaId, Estrato = "90->>"}
                };
                _context.Precios.AddRange(precios);
                    //.Add(precios);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["Fincas"] = GetFincas();
            ViewData["Incoterms"] = GetIncoterms();
            ViewData["IncotermSelected"] = facturas.Incoterm;
            return View(facturas);
        }

        // GET: Facturas/Edit/5
        public async Task<IActionResult> Edit(int? id, DateTime fecini, DateTime fecfin)
        {
            var _fecini = fecini.Year < 1900 ? new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1) : fecini.Date;
            var _fecfin = fecfin.Year < 1900 ? new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month)) : fecfin.Date;
            ViewData["FecIni"] = _fecini.ToString("yyyy-MM-dd");
            ViewData["FecFin"] = _fecfin.ToString("yyyy-MM-dd");
            if (id == null)
            {
                return NotFound();
            }

            var facturas = await _context.Facturas.SingleOrDefaultAsync(m => m.Id == id);
            if (facturas == null)
            {
                return NotFound();
            }
            ViewData["Fincas"] = GetFincas();
            ViewData["FincaSelected"] = facturas.Finca;
            ViewData["Incoterms"] = GetIncoterms();
            ViewData["IncotermSelected"] = facturas.Incoterm;

            return View(facturas);
        }

        // POST: Facturas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,FacturaId,Fecha,Cliente,Especie,Finca,Incoterm,Destino,BillOfLading")] Facturas facturas, DateTime fecini, DateTime fecfin)
        {
            var _fecini = fecini.Year < 1900 ? new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1) : fecini.Date;
            var _fecfin = fecfin.Year < 1900 ? new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month)) : fecfin.Date;
            ViewData["FecIni"] = _fecini.ToString("yyyy-MM-dd");
            ViewData["FecFin"] = _fecfin.ToString("yyyy-MM-dd");
            if (id != facturas.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(facturas);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!FacturasExists(facturas.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index),new {fecini = _fecini, fecfin = _fecfin });
            }
            return View(facturas);
        }

        // GET: Facturas/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var facturas = await _context.Facturas
                .SingleOrDefaultAsync(m => m.Id == id);
            if (facturas == null)
            {
                return NotFound();
            }

            return View(facturas);
        }

        // POST: Facturas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var facturas = await _context.Facturas.SingleOrDefaultAsync(m => m.Id == id);
            _context.Facturas.Remove(facturas);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool FacturasExists(int id)
        {
            return _context.Facturas.Any(e => e.Id == id);
        }

        private List<SelectListItem> GetFincas()
        {
            List<SelectListItem> listaFincas = new List<SelectListItem>();

            var fincas = _context.Rodales.OrderBy(o => o.Finca).GroupBy(m => m.Finca).ToList();
            foreach( var finca in fincas)
            {
                listaFincas.Add(new SelectListItem() { Value = finca.Key, Text = finca.Key });
            }
            return listaFincas;
        }

        private List<SelectListItem> GetIncoterms()
        {
            List<SelectListItem> incoterms = new List<SelectListItem>();
            incoterms.Add(new SelectListItem { Value = "FOB", Text = "FOB" });
            incoterms.Add(new SelectListItem { Value = "CIF", Text = "CIF" });
            incoterms.Add(new SelectListItem { Value = "EXW", Text = "EXW" });
            incoterms.Add(new SelectListItem { Value = "CPT", Text = "CPT" });

            return incoterms;
        }

        public async Task<IActionResult> ImportFromExcel(IFormFile theExcel)
        {
            if(theExcel == null)
            {
                return BadRequest();
            }

            ExcelPackage excel;
            using (var memoryStream = new MemoryStream())
            {
                await theExcel.CopyToAsync(memoryStream);
                excel = new ExcelPackage(memoryStream);
            }
            var workSheet = excel.Workbook.Worksheets[0];

            var start = workSheet.Dimension.Start;
            var end = workSheet.Dimension.End;
            for (int row = start.Row + 1; row <= end.Row; row++)
            {
                if (workSheet.Cells[row, 1].Value != null)
                {
                    PackingList packingList = new PackingList
                    {
                        Consecutivo = Convert.ToInt32(workSheet.Cells[row, 1].Value),
                        Clase = workSheet.Cells[row, 2].Text,
                        Ubicacion = workSheet.Cells[row, 3].Text,
                        Fecha = (DateTime)workSheet.Cells[row, 4].Value,
                        ContenedorId =  workSheet.Cells[row, 5].Text,
                        DescuentoCrc = Convert.ToSingle(workSheet.Cells[row, 6].Value),
                        DescuentoLong = Convert.ToSingle(workSheet.Cells[row, 7].Value),
                        PackingListId = workSheet.Cells[row, 8].Text,

                    };

                    if(packingList.ContenedorId.Equals(""))
                    {
                        packingList.ContenedorId = null;
                    }

                    _context.PackingList.Add(packingList);
                }
            }

            _context.SaveChanges();

            var workSheet2 = excel.Workbook.Worksheets[1];
            var start2 = workSheet2.Dimension.Start;
            var end2 = workSheet2.Dimension.End;
            for (int row = start2.Row + 1; row <= end2.Row; row++)
            {
                if (workSheet2.Cells[row, 1].Value != null)
                {
                    DetalleContenedores detalleContenedor = new DetalleContenedores
                    {
                        Circunferencia = Convert.ToSingle(workSheet2.Cells[row, 1].Value),
                        Longitud = Convert.ToSingle(workSheet2.Cells[row, 2].Value),
                        PackingListId = workSheet2.Cells[row, 3].Text,
                    };

                    _context.DetalleContenedores.Add(detalleContenedor);

                }
            }
            _context.SaveChanges();
            return RedirectToAction(nameof(PackingList), new { origen = 2 });

        }
        // PATCH: api/PackingList/5
        [HttpPatch("{Id}")]
        public async Task<IActionResult> PatchPackingListAsync([FromRoute] string id, [FromBody] JsonPatchDocument<PackingList> patchDoc)
        {
            if (patchDoc == null)
            {
                return BadRequest();
            }

            //In your application get Object review object from database base on review Id
            var packingListFromDb = await _context.PackingList.SingleOrDefaultAsync(m => m.PackingListId == id);    

            if (packingListFromDb == null)
            {
                return NotFound();
            }

            //Apply changes to Object
            patchDoc.ApplyTo(packingListFromDb);

            // Say to entity framework that you have changes in book entity and it's modified
            _context.Entry(packingListFromDb).State = EntityState.Modified;

            // Save changes to database
            await _context.SaveChangesAsync();

            //update Object in database and return it updated 
            return Ok(packingListFromDb);

        }
    }
}
