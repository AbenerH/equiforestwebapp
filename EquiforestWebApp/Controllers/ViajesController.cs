﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EquiforestWebApp.Data;
using EquiforestWebApp.Models;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System.IO;

namespace EquiforestWebApp.Controllers
{
    public class ViajesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly TrozasController _trozasController;

        public ViajesController(ApplicationDbContext context)
        {
            _context = context;
            _trozasController = new TrozasController(context);
        }

        // GET: Viajes
        public IActionResult Index(int? pageNumber, DateTime fecini, DateTime fecfin, bool download, string rodal, string finca)
        {
            int pageSize = 15;
            var _fecini = fecini.Year < 1900 ? new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1) : fecini.Date;
            var _fecfin = fecfin.Year < 1900 ? new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month)) : fecfin.Date;
            ViewData["FecIni"] = _fecini.ToString("yyyy-MM-dd");
            ViewData["FecFin"] = _fecfin.ToString("yyyy-MM-dd");

            List<ViajesGroup> viajesgroupList = new List<ViajesGroup>();
            if (finca == null && rodal == null)
            {
                var result = from s in _context.Viajes
                             join t in _context.Trozas
                             on s.ViajeId equals t.ViajeId into joinViajes
                             where s.FechaIngreso >= _fecini && s.FechaIngreso <= _fecfin
                             from tr in joinViajes.DefaultIfEmpty()
                             select new
                             {
                                 Nucleo = s.Nucleo,
                                 Finca = s.Finca,
                                 Rodal = s.Rodal,
                                 Fecha = s.FechaIngreso,
                                 Ubicacion = s.Ubicacion,
                                 N_Viaje = (int)s.N_Viaje,
                                 NoTrozasC = s.NoTrozasC,
                                 Clasificacion = tr == null ? "" : tr.Clasificacion,
                                 Circunferencia = tr == null ? 0f : tr.Circunferencia,
                                 Longitud = tr == null ? 0f : tr.Largo,
                                 Volumen = tr == null ? 0f : MathF.Round((float)(((tr.Circunferencia * tr.Circunferencia) / (4 * Math.PI)) * tr.Largo / 10000), 2)
                             };
                var viajesgroupresult = result.OrderBy(o => o.Fecha).ThenBy(o => o.N_Viaje).ThenBy(o => o.Clasificacion).GroupBy(g => new { g.Nucleo, g.Finca, g.Rodal, g.Fecha, g.Ubicacion, g.N_Viaje, g.Clasificacion });
                foreach (var viaje in viajesgroupresult)
                {
                    viajesgroupList.Add(new ViajesGroup
                    {
                        Nucleo = viaje.Key.Nucleo,
                        Finca = viaje.Key.Finca,
                        Rodal = viaje.Key.Rodal,
                        Fecha = viaje.Key.Fecha,
                        Ubicacion = viaje.Key.Ubicacion,
                        N_Viaje = viaje.Key.N_Viaje,
                        Clasificacion = viaje.Key.Clasificacion,
                        Circunferencia = viaje.Average(c => c.Circunferencia),
                        Longitud = viaje.Average(l => l.Longitud),
                        Cantidad = viaje.Sum(v => v.Volumen) > 0 ? viaje.Count() : 0,
                        Volumen = viaje.Sum(v => v.Volumen),
                        NoTrozasC = viaje.Max(t => t.NoTrozasC)
                    });
                }
            }
            else if (finca != null && rodal == null)
            {
                var result = from s in _context.Viajes
                             join t in _context.Trozas
                             on s.ViajeId equals t.ViajeId into joinViajes
                             where s.FechaIngreso >= _fecini && s.FechaIngreso <= _fecfin && s.Finca == finca
                             from tr in joinViajes.DefaultIfEmpty()
                             select new
                             {
                                 Nucleo = s.Nucleo,
                                 Finca = s.Finca,
                                 Rodal = s.Rodal,
                                 Fecha = s.FechaIngreso,
                                 Ubicacion = s.Ubicacion,
                                 N_Viaje = (int)s.N_Viaje,
                                 NoTrozasC = s.NoTrozasC,
                                 Clasificacion = tr == null ? "" : tr.Clasificacion,
                                 Circunferencia = tr == null ? 0f : tr.Circunferencia,
                                 Longitud = tr == null ? 0f : tr.Largo,
                                 Volumen = tr == null ? 0f : MathF.Round((float)(((tr.Circunferencia * tr.Circunferencia) / (4 * Math.PI)) * tr.Largo / 10000), 2)
                             };
                var viajesgroupresult = result.OrderBy(o => o.Fecha).ThenBy(o => o.N_Viaje).ThenBy(o => o.Clasificacion).GroupBy(g => new { g.Nucleo, g.Finca, g.Rodal, g.Fecha, g.Ubicacion, g.N_Viaje, g.Clasificacion });
                foreach (var viaje in viajesgroupresult)
                {
                    viajesgroupList.Add(new ViajesGroup
                    {
                        Nucleo = viaje.Key.Nucleo,
                        Finca = viaje.Key.Finca,
                        Rodal = viaje.Key.Rodal,
                        Fecha = viaje.Key.Fecha,
                        Ubicacion = viaje.Key.Ubicacion,
                        N_Viaje = viaje.Key.N_Viaje,
                        Clasificacion = viaje.Key.Clasificacion,
                        Circunferencia = viaje.Average(c => c.Circunferencia),
                        Longitud = viaje.Average(l => l.Longitud),
                        Cantidad = viaje.Sum(v => v.Volumen) > 0 ? viaje.Count() : 0,
                        Volumen = viaje.Sum(v => v.Volumen),
                        NoTrozasC = viaje.Max(t => t.NoTrozasC)
                    });
                }
            }
            else if (rodal != null)
            {
                var result = from s in _context.Viajes
                             join t in _context.Trozas
                             on s.ViajeId equals t.ViajeId into joinViajes
                             where s.FechaIngreso >= _fecini && s.FechaIngreso <= _fecfin && s.Rodal == rodal
                             from tr in joinViajes.DefaultIfEmpty()
                             select new
                             {
                                 Nucleo = s.Nucleo,
                                 Finca = s.Finca,
                                 Rodal = s.Rodal,
                                 Fecha = s.FechaIngreso,
                                 Ubicacion = s.Ubicacion,
                                 N_Viaje = (int)s.N_Viaje,
                                 NoTrozasC = s.NoTrozasC,
                                 Clasificacion = tr == null ? "" : tr.Clasificacion,
                                 Circunferencia = tr == null ? 0f : tr.Circunferencia,
                                 Longitud = tr == null ? 0f : tr.Largo,
                                 Volumen = tr == null ? 0f : MathF.Round((float)(((tr.Circunferencia * tr.Circunferencia) / (4 * Math.PI)) * tr.Largo / 10000), 2)
                             };
                var viajesgroupresult = result.OrderBy(o => o.Fecha).ThenBy(o => o.N_Viaje).ThenBy(o => o.Clasificacion).GroupBy(g => new { g.Nucleo, g.Finca, g.Rodal, g.Fecha, g.Ubicacion, g.N_Viaje, g.Clasificacion });
                foreach (var viaje in viajesgroupresult)
                {
                    viajesgroupList.Add(new ViajesGroup
                    {
                        Nucleo = viaje.Key.Nucleo,
                        Finca = viaje.Key.Finca,
                        Rodal = viaje.Key.Rodal,
                        Fecha = viaje.Key.Fecha,
                        Ubicacion = viaje.Key.Ubicacion,
                        N_Viaje = viaje.Key.N_Viaje,
                        Clasificacion = viaje.Key.Clasificacion,
                        Circunferencia = viaje.Average(c => c.Circunferencia),
                        Longitud = viaje.Average(l => l.Longitud),
                        Cantidad = viaje.Sum(v => v.Volumen) > 0 ? viaje.Count() : 0,
                        Volumen = viaje.Sum(v => v.Volumen),
                        NoTrozasC = viaje.Max(t => t.NoTrozasC)
                    });
                }
            }

            //return View(await _context.Trozas.ToListAsync());
            if (download)
            {
                Download(viajesgroupList, "Viajes.xlsx");
            }
            var fincas = GetFincas();
            ViewData["Fincas"] = fincas;
            ViewData["FincaSelected"] = finca;
            var rodales = GetRodales();
            ViewData["Rodales"] = rodales;
            var _finca = "";
            if (rodal != null)
            {
                _finca = rodales.Find(m => m.Value == rodal).Text;
            }
            ViewData["RodalSelected"] = rodal;
            ViewData["RodalSelectedText"] = _finca;

            return View(PaginatedList<ViajesGroup>.Create(viajesgroupList.AsQueryable(), pageNumber ?? 1, pageSize));
            //return View(await PaginatedList<Trozas>.CreateAsync(_context.Trozas.Include(t => t.Viaje).Where(f => f.Viaje.FechaIngreso >= _fecini && f.Viaje.FechaIngreso <= _fecfin).AsNoTracking(), pageNumber ?? 1, pageSize));

            //return View(await _context.Viajes.ToListAsync());
        }

        public IActionResult Viaje(int? pageNumber, DateTime fecini, DateTime fecfin, string rodal, string finca)
        {
            int pageSize = 15;
            var _fecini = fecini.Year < 1900 ? new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1) : fecini.Date;
            var _fecfin = fecfin.Year < 1900 ? new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month)) : fecfin.Date;
            ViewData["FecIni"] = _fecini.ToString("yyyy-MM-dd");
            ViewData["FecFin"] = _fecfin.ToString("yyyy-MM-dd");

            List<Viajes> viajesgroupList = new List<Viajes>();
            if (finca == null && rodal == null)
            {
                var result = from s in _context.Viajes
                             join t in _context.Trozas
                             on s.ViajeId equals t.ViajeId into joinViajes
                             where s.FechaIngreso >= _fecini && s.FechaIngreso <= _fecfin
                             from tr in joinViajes.DefaultIfEmpty()
                             select new
                             {
                                 Id = s.Id,
                                 ViajeId = s.ViajeId,
                                 Nucleo = s.Nucleo,
                                 Finca = s.Finca,
                                 Rodal = s.Rodal,
                                 Fecha = s.FechaIngreso,
                                 Ubicacion = s.Ubicacion,
                                 N_Viaje = (int)s.N_Viaje,
                                 NoTrozasC = s.NoTrozasC,
                                 Clasificacion = tr == null ? "" : tr.Clasificacion,
                                 Circunferencia = tr == null ? 0f : tr.Circunferencia,
                                 Longitud = tr == null ? 0f : tr.Largo,
                                 Volumen = tr == null ? 0f : MathF.Round((float)(((tr.Circunferencia * tr.Circunferencia) / (4 * Math.PI)) * tr.Largo / 10000), 2)
                             };
                var viajesgroupresult = result.OrderBy(o => o.Fecha).ThenBy(o => o.N_Viaje).GroupBy(g => new { g.Id, g.ViajeId, g.Nucleo, g.Finca, g.Rodal, g.Fecha, g.Ubicacion, g.N_Viaje });
                foreach (var viaje in viajesgroupresult)
                {
                    viajesgroupList.Add(new Viajes
                    {
                        Id = viaje.Key.Id,
                        ViajeId = viaje.Key.ViajeId,
                        Nucleo = viaje.Key.Nucleo,
                        Finca = viaje.Key.Finca,
                        Rodal = viaje.Key.Rodal,
                        FechaIngreso = viaje.Key.Fecha,
                        Ubicacion = viaje.Key.Ubicacion,
                        N_Viaje = viaje.Key.N_Viaje,
                        NoTrozasC = viaje.Max(t => t.NoTrozasC)
                    });
                }
            }
            else if (finca != null && rodal == null)
            {
                var result = from s in _context.Viajes
                             join t in _context.Trozas
                             on s.ViajeId equals t.ViajeId into joinViajes
                             where s.FechaIngreso >= _fecini && s.FechaIngreso <= _fecfin && s.Finca == finca
                             from tr in joinViajes.DefaultIfEmpty()
                             select new
                             {
                                 ViajeId = s.ViajeId,
                                 Id = s.Id,
                                 Nucleo = s.Nucleo,
                                 Finca = s.Finca,
                                 Rodal = s.Rodal,
                                 Fecha = s.FechaIngreso,
                                 Ubicacion = s.Ubicacion,
                                 N_Viaje = (int)s.N_Viaje,
                                 NoTrozasC = s.NoTrozasC,
                                 Clasificacion = tr == null ? "" : tr.Clasificacion,
                                 Circunferencia = tr == null ? 0f : tr.Circunferencia,
                                 Longitud = tr == null ? 0f : tr.Largo,
                                 Volumen = tr == null ? 0f : MathF.Round((float)(((tr.Circunferencia * tr.Circunferencia) / (4 * Math.PI)) * tr.Largo / 10000), 2)
                             };
                var viajesgroupresult = result.OrderBy(o => o.Fecha).GroupBy(g => new { g.Id, g.ViajeId, g.Nucleo, g.Finca, g.Rodal, g.Fecha, g.Ubicacion, g.N_Viaje });
                foreach (var viaje in viajesgroupresult)
                {
                    viajesgroupList.Add(new Viajes
                    {
                        ViajeId = viaje.Key.ViajeId,
                        Id = viaje.Key.Id,
                        Nucleo = viaje.Key.Nucleo,
                        Finca = viaje.Key.Finca,
                        Rodal = viaje.Key.Rodal,
                        FechaIngreso = viaje.Key.Fecha,
                        Ubicacion = viaje.Key.Ubicacion,
                        N_Viaje = viaje.Key.N_Viaje,
                        NoTrozasC = viaje.Max(t => t.NoTrozasC)
                    });
                }
            }
            else if (rodal != null)
            {
                var result = from s in _context.Viajes
                             join t in _context.Trozas
                             on s.ViajeId equals t.ViajeId into joinViajes
                             where s.FechaIngreso >= _fecini && s.FechaIngreso <= _fecfin && s.Rodal == rodal
                             from tr in joinViajes.DefaultIfEmpty()
                             select new
                             {
                                 ViajeId = s.ViajeId,
                                 Id = s.Id,
                                 Nucleo = s.Nucleo,
                                 Finca = s.Finca,
                                 Rodal = s.Rodal,
                                 Fecha = s.FechaIngreso,
                                 Ubicacion = s.Ubicacion,
                                 N_Viaje = (int)s.N_Viaje,
                                 NoTrozasC = s.NoTrozasC,
                                 Clasificacion = tr == null ? "" : tr.Clasificacion,
                                 Circunferencia = tr == null ? 0f : tr.Circunferencia,
                                 Longitud = tr == null ? 0f : tr.Largo,
                                 Volumen = tr == null ? 0f : MathF.Round((float)(((tr.Circunferencia * tr.Circunferencia) / (4 * Math.PI)) * tr.Largo / 10000), 2)
                             };
                var viajesgroupresult = result.OrderBy(o => o.Fecha).GroupBy(g => new { g.Id, g.ViajeId, g.Nucleo, g.Finca, g.Rodal, g.Fecha, g.Ubicacion, g.N_Viaje });
                foreach (var viaje in viajesgroupresult)
                {
                    viajesgroupList.Add(new Viajes
                    {
                        ViajeId = viaje.Key.ViajeId,
                        Id = viaje.Key.Id,
                        Nucleo = viaje.Key.Nucleo,
                        Finca = viaje.Key.Finca,
                        Rodal = viaje.Key.Rodal,
                        FechaIngreso = viaje.Key.Fecha,
                        Ubicacion = viaje.Key.Ubicacion,
                        N_Viaje = viaje.Key.N_Viaje,
                        NoTrozasC = viaje.Max(t => t.NoTrozasC)
                    });
                }
            }

            var fincas = GetFincas();
            ViewData["Fincas"] = fincas;
            ViewData["FincaSelected"] = finca;
            var rodales = GetRodales();
            ViewData["Rodales"] = rodales;
            var _finca = "";
            if (rodal != null)
            {
                _finca = rodales.Find(m => m.Value == rodal).Text;
            }
            ViewData["RodalSelected"] = rodal;
            ViewData["RodalSelectedText"] = _finca;

            return View(PaginatedList<Viajes>.Create(viajesgroupList.AsQueryable(), pageNumber ?? 1, pageSize));
            //return View(PaginatedList<Viajes>.Create(_context.Viajes, pageNumber ?? 1, pageSize));
        }


        // GET: Viajes/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var viajes = await _context.Viajes
                .SingleOrDefaultAsync(m => m.ViajeId == id);
            if (viajes == null)
            {
                return NotFound();
            }

            return View(viajes);
        }

        /* GET: Viajes/Create
         
        public IActionResult Create()
        {
            //Do Something


            return View();
        }

         POST: Viajes/Create
         To protect from overposting attacks, please enable the specific properties you want to bind to, for 
         more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Nucleo,Finca,Rodal,FechaIngreso,Ubicacion,N_Viaje")] Viajes viajes)
        {
            viajes.ViajeId = viajes.Rodal + viajes.N_Viaje.ToString() + viajes.FechaIngreso.Month.ToString() + viajes.FechaIngreso.Day.ToString() + viajes.FechaIngreso.Year.ToString();

            if (ModelState.IsValid)
            {
                _context.Viajes.Add(viajes);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            
            return View(viajes);
        }

    */

        public IActionResult OnGetViajesCreate()
        {
            //GetSelectedListItem();
            var model = new Viajes();
            model.Nucleos = GetNucleos();
            model.Fincas = GetFincasCreate();
            model.Rodales = GetRodales();
            model.FechaIngreso = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day); 
            return PartialView("/Views/Viajes/_ViajesCreate.cshtml", model);
        }

        public IActionResult OnGetViajesEdit(int id)
        {
            //GetSelectedListItem();
            var model =  _context.Viajes.SingleOrDefault(m => m.Id == id);
            model.Rodales = GetRodales();
//            ViewData["Rodales"] = _context.Rodales.ToList();
            return PartialView("/Views/Viajes/_ViajesEdit.cshtml", model);
        }

        public Rodales UpdateRodalesFields(string rodal)
        {
            var model = _context.Rodales.FirstOrDefault(m => m.Rodal == rodal);

            return model;
        }

        public async Task<IActionResult> OnPostViajesCreate([Bind("Nucleo,Finca,Rodal,FechaIngreso,Ubicacion,N_Viaje, NoTrozasC")] Viajes viajes)
        {
            viajes.ViajeId = viajes.Rodal + getNewNViaje(viajes.N_Viaje) + getNewDate(viajes.FechaIngreso.Month.ToString()) + getNewDate(viajes.FechaIngreso.Day.ToString()) + viajes.FechaIngreso.Year.ToString();
            
//           viajes.Nucleos = GetNucleos();
//           viajes.Fincas = GetFincas();
//           viajes.Rodales = GetRodales();
                                                                                                                                                                                                                                                                                                                                                  
            Console.WriteLine(viajes.ToString());

            if (ModelState.IsValid)
            {
                _context.Viajes.Add(viajes);
                await _context.SaveChangesAsync();
                //return RedirectToAction(nameof(Index));
                return PartialView("/Views/Viajes/_ViajesCreate.cshtml", viajes);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        private string getNewNViaje(int? n_Viaje)
        {

            string newViaje = "000" + n_Viaje.ToString();

            return newViaje.Substring(newViaje.Length - 3);
        }

        private string getNewDate(string date)
        {
            int numeric = int.Parse(date);

            if(numeric < 10)
            {
                date = "0" + date;
            }

            return date;

        }

/*        public async Task<IActionResult> OnPutViajesCreate([Bind("Nucleo,Finca,Rodal,FechaIngreso,Ubicacion,N_Viaje")] Viajes viajes)
        {
            viajes.ViajeId = viajes.Rodal + getNewNViaje(viajes.N_Viaje) + getNewDate(viajes.FechaIngreso.Month.ToString()) + getNewDate(viajes.FechaIngreso.Day.ToString()) + viajes.FechaIngreso.Year.ToString();



            if (ModelState.IsValid)
            {
                _context.Entry(viajes).State = EntityState.Modified;
                //_context.Update(viajes);
                try
                {
                    await _context.SaveChangesAsync();

                }catch(Exception e)
                {
                    Console.WriteLine("Error" + e.Message);
                }
                //return RedirectToAction(nameof(Index));
            }

            return PartialView("/Views/Viajes/_ViajesCreate.cshtml", viajes);

        } */

        public async Task<IActionResult> GetViaje(int id)
        {
            //GetSelectedListItem();
            var model = await _context.Viajes.SingleOrDefaultAsync(m => m.Id == id);
            return PartialView("/Views/Viajes/_ViajesEdit.cshtml",model);
        }


        public async Task<IActionResult> EditModal(int id, Viajes viajes)
        {
            if (id != viajes.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                viajes.ViajeId = viajes.Rodal + getNewNViaje(viajes.N_Viaje) + getNewDate(viajes.FechaIngreso.Month.ToString()) + getNewDate(viajes.FechaIngreso.Day.ToString()) + viajes.FechaIngreso.Year.ToString();

                try
                {
                    _context.Entry(viajes).State = EntityState.Modified;
                    _context.Update(viajes);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ViajesExists(viajes.ViajeId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(viajes);
        }


        // GET: Viajes/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var viajes = await _context.Viajes.SingleOrDefaultAsync(m => m.ViajeId == id);
            if (viajes == null)
            {
                return NotFound();
            }
            return View(viajes);
        }

        // POST: Viajes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("Id,ViajeId,Nucleo,Finca,Rodal,FechaIngreso,Ubicacion,N_Viaje")] Viajes viajes)
        {
            if (id != viajes.ViajeId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(viajes);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ViajesExists(viajes.ViajeId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(viajes);
        }

        // GET: Viajes/Delete/5
        public async Task<IActionResult> Delete(int? id, DateTime fecini, DateTime fecfin, string rodal, string finca)
        {
            if (id == null)
            {
                return NotFound();
            }

            var _fecini = fecini.Year < 1900 ? new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1) : fecini.Date;
            var _fecfin = fecfin.Year < 1900 ? new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month)) : fecfin.Date;
            ViewData["FecIni"] = _fecini.ToString("yyyy-MM-dd");
            ViewData["FecFin"] = _fecfin.ToString("yyyy-MM-dd");

            var fincas = GetFincas();
            ViewData["Fincas"] = fincas;
            ViewData["FincaSelected"] = finca;
            var rodales = GetRodales();
            ViewData["Rodales"] = rodales;
            var _finca = "";
            if (rodal != null)
            {
                _finca = rodales.Find(m => m.Value == rodal).Text;
            }
            ViewData["RodalSelected"] = rodal;
            ViewData["RodalSelectedText"] = _finca;

            var viaje = await _context.Viajes
                .SingleOrDefaultAsync(m => m.Id == id);

            if (viaje == null)
            {
                return NotFound();
            }

            //Delete All Trozas in this viaje
            //try
            //{
            //    await deleteTrozasFromViaje(id);
            //}
            //catch(Exception e)
            //{
            //    Console.Write(e.Message);
            //}

            //Delete Viaje
            try
            {
                _context.Viajes.Remove(viaje);
                await _context.SaveChangesAsync();

            }
            catch (Exception e)
            {
                Console.Write(e.Message);
            }

            return RedirectToAction(nameof(Viaje), new { fecini = _fecini, fecfin = _fecfin });
        }

        // private async Task<IActionResult> deleteTrozasFromViaje(string id)
        //{

        //    return await _trozasController.DeletebyViajeId(id);

        //}

        // POST: Viajes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var viajes = await _context.Viajes.SingleOrDefaultAsync(m => m.ViajeId == id);
            _context.Viajes.Remove(viajes);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ViajesExists(string id)
        {
            return _context.Viajes.Any(e => e.ViajeId == id);
        }

        private List<SelectListItem> GetNucleos()
        {
            var rodales = from s in _context.Rodales select s;
            var nucleos = rodales.GroupBy(g => g.Nucleo).Select(s => new { Nucleo = s.Key }).ToList();
            var nucleosList = new List<SelectListItem>();
            foreach (var emp in nucleos)
            {
                nucleosList.Add(new SelectListItem { Value = emp.Nucleo, Text = emp.Nucleo });
            }

            return nucleosList;
        }

        private List<SelectListItem> GetFincasCreate()
        {
            var rodales = from s in _context.Rodales select s;
            var fincas = rodales.GroupBy(g => g.Finca).Select(s => new { Finca = s.Key }).ToList();
            var fincasList = new List<SelectListItem>();
            foreach (var val in fincas)
            {
                fincasList.Add(new SelectListItem { Value = val.Finca, Text = val.Finca });
            }

            return fincasList;
        }

        private List<SelectListItem> GetFincas()
        {
            var rodales = from s in _context.Rodales orderby s.Rodal select s;
            var fincas = rodales.GroupBy(m => new { m.Nucleo, m.Finca }).Select(s => new { Nucleo = s.Key.Nucleo, Finca = s.Key.Finca });
            var fincasList = new List<SelectListItem>();
            fincasList.Add(new SelectListItem { Value = "", Text = "" });
            foreach (var val in fincas)
            {
                fincasList.Add(new SelectListItem { Value = val.Finca, Text = val.Nucleo + " - " + val.Finca });
            }

            return fincasList;
        }

        private List<SelectListItem> GetRodales()
        {

            var rodales = from s in _context.Rodales orderby s.Rodal select s;

            var rodalesList = new List<SelectListItem>();
            rodalesList.Add(new SelectListItem { Value = "", Text = "" });
            foreach (var val in rodales)
            {
                rodalesList.Add(new SelectListItem { Value = val.Rodal, Text = val.Rodal + " - " + val.Finca });
            }

            return rodalesList;
        }


        public async Task<IActionResult> ImportFromExcel(IFormFile theExcel, int Id, int empresaId)
        {
            ExcelPackage excel;
            using (var memoryStream = new MemoryStream())
            {
                await theExcel.CopyToAsync(memoryStream);
                excel = new ExcelPackage(memoryStream);
            }
            var workSheet = excel.Workbook.Worksheets[0];
            var start = workSheet.Dimension.Start;
            var end = workSheet.Dimension.End;
            for (int row = start.Row + 1; row <= end.Row; row++)
            {
                if (workSheet.Cells[row, 1].Value != null)
                {
                    Viajes a = new Viajes
                    {
                        ViajeId = workSheet.Cells[row, 1].Text,
                        Nucleo = workSheet.Cells[row,3].Text,
                        Finca = workSheet.Cells[row, 4].Text,
                        Rodal = workSheet.Cells[row, 5].Text,
                        FechaIngreso = (DateTime)workSheet.Cells[row, 2].Value,
                        Ubicacion = workSheet.Cells[row, 6].Text,
                        N_Viaje = Convert.ToInt32(workSheet.Cells[row, 7].Value),
                        NoTrozasC = Convert.ToInt32(workSheet.Cells[row, 8].Value)
                    };
                    _context.Viajes.Add(a);
                }
            }
            _context.SaveChanges();

            var workSheet2 = excel.Workbook.Worksheets[1];
            var start2 = workSheet2.Dimension.Start;
            var end2 = workSheet2.Dimension.End;
            for (int row = start2.Row + 1; row <= end2.Row; row++)
            {
                if (workSheet2.Cells[row, 1].Value != null)
                {
                    Trozas a = new Trozas
                    {
                        Clasificacion = workSheet2.Cells[row, 1].Text,
                        Circunferencia = Convert.ToSingle(workSheet2.Cells[row, 2].Value),
                        Largo = Convert.ToSingle(workSheet2.Cells[row, 3].Value),
                        ViajeId = workSheet2.Cells[row, 4].Text
                    };
                    _context.Trozas.Add(a);
                }
            }
            _context.SaveChanges();
            return RedirectToAction(nameof(Index));
        }

        public void Download(List<ViajesGroup> data, string name)
        {
            int numRows = data.Count();
            if (numRows > 0)
            {
                var stream = new MemoryStream();
                ExcelPackage excel = new ExcelPackage(stream);
                var workSheet = excel.Workbook.Worksheets.Add("Data");
                workSheet.Cells[1, 1].LoadFromCollection(data, true);
                workSheet.Cells.AutoFitColumns();
                using (var memoryStream = new MemoryStream())
                {
                    Response.Clear();
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.Headers["content-disposition"] = "attachment; filename=" + name;
                    excel.SaveAs(memoryStream);
                    memoryStream.WriteTo(Response.Body);
                }
            }

        }
    }
}
