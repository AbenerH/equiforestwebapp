﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EquiforestWebApp.Data;
using EquiforestWebApp.Models;

namespace EquiforestWebApp.Controllers
{
    public class TrozasController : Controller
    {
        private readonly ApplicationDbContext _context;

        public TrozasController(ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult OnGetTrozasEdit(int id)
        {
            //GetSelectedListItem();
            var model = _context.Trozas.SingleOrDefault(m => m.Id == id);
            return PartialView("/Views/Trozas/_TrozasEdit.cshtml", model);
        }

        // GET: Trozas
        public async Task<IActionResult> Index(int? pageNumber, string id)
        {

            ViewData["viajeId"] = id;

            int pageSize = 10;
            //return View(await _context.Trozas.ToListAsync());
            return View(await PaginatedList<Trozas>.CreateAsync(_context.Trozas.Where(f => f.ViajeId == id).AsNoTracking().AsQueryable(), pageNumber ?? 1, pageSize));
        }

        //public async Task<IActionResult> Trozas(int? pageNumber, DateTime fecini, DateTime fecfin)
        //{

        //}

        public async Task<IActionResult> Resumen()
        {
            //var result = from s in _context.Trozas join v in _context.Viajes on s.ViajesId equals v.Id
            //             select new ResumenCosecha { Circunferencia = s.Circunferencia, Clasificacion = s.Clasificacion, Largo = s.Largo, Volumen = ((Math.Sqrt((Double)s.Circunferencia))/(4*Math.PI))*s.Largo/10000 };
            // return View(await result.AsNoTracking().ToListAsync());
            return View(await _context.Trozas.AsNoTracking().ToListAsync());
        }

        // GET: Trozas/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var trozas = await _context.Trozas
                .SingleOrDefaultAsync(m => m.Id == id);
            if (trozas == null)
            {
                return NotFound();
            }

            return View(trozas);
        }

        // GET: Trozas/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Trozas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Clasificacion,Circunferencia,Largo")] Trozas trozas, string id)
        {
            if (ModelState.IsValid)
            {
                trozas.ViajeId = id;
                trozas.ViajeId = id;
                _context.Add(trozas);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index), new { id = id });
            }

            return View(trozas);
        }

        public async Task<IActionResult> EditModal(int id, Trozas trozas)
        {
            if (id != trozas.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(trozas);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {

                }
                return RedirectToAction(nameof(Index));
            }
            return View(trozas);
        }

        private bool TrozasExists(string id)
        {
            return _context.Trozas.Any(e => e.ViajeId == id);
        }

        // GET: Trozas/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var trozas = await _context.Trozas.SingleOrDefaultAsync(m => m.Id == id);
            if (trozas == null)
            {
                return NotFound();
            }
            return View(trozas);
        }

        // POST: Trozas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,IdViaje,Clasificacion,Circunferencia,Largo")] Trozas trozas)
        {
            if (id != trozas.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(trozas);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TrozasExists(trozas.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index), new { id = trozas.ViajeId } );
            }
            return View(trozas);
        }

        // GET: Trozas/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var troza = await _context.Trozas
                .SingleOrDefaultAsync(m => m.Id == id);

            if (troza == null)
            {
                return NotFound();
            }

            _context.Trozas.Remove(troza);
            await _context.SaveChangesAsync();

            //return View(troza);
            return RedirectToAction(nameof(Index), new { id = troza.ViajeId });
        }

        // GET: Trozas/Delete/123asd
                public async Task<IActionResult> DeletebyViajeId (string viajeId)
                {
                    if (viajeId == null)
                    {
                        return NotFound();
                    }

                    var trozas = _context.Trozas.Where(m => m.ViajeId == viajeId);

                    if (trozas == null)
                    {
                        return NotFound();
                    }

                    _context.Trozas.RemoveRange(trozas);
                    await _context.SaveChangesAsync();

                    return Ok(trozas);
                } 

                // POST: Trozas/Delete/5
                [HttpPost, ActionName("Delete")]
                [ValidateAntiForgeryToken]
                public async Task<IActionResult> DeleteConfirmed(int id)
                {
                    var trozas = await _context.Trozas.SingleOrDefaultAsync(m => m.Id == id);
                    _context.Trozas.Remove(trozas);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }

                private bool TrozasExists(int id)
                {
                    return _context.Trozas.Any(e => e.Id == id);
                }
            }
        }
