﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EquiforestWebApp.Models;
using EquiforestWebApp.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using System.IO;
using OfficeOpenXml;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Identity;

namespace EquiforestWebApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        //private readonly DbInitializer _dbInitializer;

        public HomeController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
           // _dbInitializer = dbInitializer;
        }

        [Authorize(Roles ="Administrator")]
        public async Task<IActionResult> UsersManager()
        {
            return View(await _context.Users.AsNoTracking().ToListAsync());
        }

        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> UserRoles(string Id, string UserName)
        {
            var result = from s in _context.UserRoles
                         join r in _context.Roles on s.RoleId equals r.Id
                         where s.UserId == Id
                         select new UserRoles
                         {
                             UserId = s.UserId,
                             RoleId = s.RoleId,
                             RoleName = r.Name
                         };
            ViewData["UserName"] = UserName;
            return View(await result.AsNoTracking().ToListAsync());
        }

        //GET the new role for the selected user
        [Authorize(Roles = "Administrator")]
        public IActionResult OnGetRoles()
        {
            var roles = _context.Roles.AsNoTracking().ToList();
            List<SelectListItem> rolesList = new List<SelectListItem>();
            foreach (var role in roles)
            {
                rolesList.Add(new SelectListItem { Value = role.Name, Text = role.Name });
            }
            return PartialView("/Views/Home/_AddRole.cshtml", rolesList);
        }

        //POST the new role for the selected user
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> SaveRole(string user, string role)
        {
            try
            {
                //_dbInitializer.SetRole(user, role);
                await _userManager.AddToRoleAsync(await _userManager.FindByNameAsync(user), role);
            }
            catch (DbUpdateConcurrencyException)
            {

            }
            return RedirectToAction(nameof(UserRoles));
        }

        [Authorize]
        public async Task<IActionResult> ResumenCosecha(DateTime fecini, DateTime fecfin, string rodal, string finca)
        {
            var _fecini = fecini.Year < 1900 ? new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1) : fecini.Date;
            var _fecfin = fecfin.Year < 1900 ? new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month)) : fecfin.Date;
            ViewData["FecIni"] = _fecini.ToString("yyyy-MM-dd");
            ViewData["FecFin"] = _fecfin.ToString("yyyy-MM-dd");
            /*var result = from s in _context.Trozas
                          join v in _context.Viajes on s.ViajeId equals v.ViajeId
                         where v.FechaIngreso>=_fecini && v.FechaIngreso <= _fecfin
                         select new ResumenCosecha { Circunferencia = s.Circunferencia, Clasificacion = s.Clasificacion, Largo = s.Largo,
                             Volumen =MathF.Round((float)(((s.Circunferencia* s.Circunferencia) / (4 * Math.PI)) * s.Largo / 10000),2),
                         NoTrozasC = v.NoTrozasC, VolTrozasC = 0.0f};
                         */
            List<ResumenCosecha> model = null;
            if (rodal==null && finca == null)
            {
                var result = from v in _context.Viajes
                         join s in _context.Trozas on v.ViajeId equals s.ViajeId into joinViajes
                         where v.FechaIngreso >= _fecini && v.FechaIngreso <= _fecfin
                         from vi in joinViajes.DefaultIfEmpty()
                         select new ResumenCosecha
                         {
                             Circunferencia = vi == null ? 0f : vi.Circunferencia,
                             Clasificacion = vi==null ? "none" : vi.Clasificacion,
                             Largo = vi == null ? 0f : vi.Largo,
                             Volumen = vi == null ? 0 : MathF.Round((float)(((vi.Circunferencia * vi.Circunferencia) / (4 * Math.PI)) * vi.Largo / 10000), 2),
                             NoTrozasC = v.NoTrozasC,
                             VolTrozasC = 0.0f
                         };
                var viajes = from v in _context.Viajes where v.FechaIngreso >= _fecini && v.FechaIngreso <= _fecfin select v;
                var noTrozasC = viajes.Sum(c => c.NoTrozasC);
                if (noTrozasC == null) noTrozasC = 0;
                ViewData["NoViajes"] = viajes.Count();
                ViewData["NoTrozasC"] = noTrozasC;
                ViewData["VolTrozasC"] = Math.Round((decimal)(noTrozasC * 0.0197683239962652), 2);
                model = result.AsNoTracking().ToList();
            }
            else if (finca!=null && rodal==null)
            {
                var result = from v in _context.Viajes
                             join s in _context.Trozas on v.ViajeId equals s.ViajeId into joinViajes
                             where v.FechaIngreso >= _fecini && v.FechaIngreso <= _fecfin && v.Finca == finca
                             from vi in joinViajes.DefaultIfEmpty()
                             select new ResumenCosecha
                             {
                                 Circunferencia = vi == null ? 0f : vi.Circunferencia,
                                 Clasificacion = vi == null ? "none" : vi.Clasificacion,
                                 Largo = vi == null ? 0f : vi.Largo,
                                 Volumen = vi == null ? 0 : MathF.Round((float)(((vi.Circunferencia * vi.Circunferencia) / (4 * Math.PI)) * vi.Largo / 10000), 2),
                                 NoTrozasC = v.NoTrozasC,
                                 VolTrozasC = 0.0f
                             };
                var viajes = from v in _context.Viajes where v.FechaIngreso >= _fecini && v.FechaIngreso <= _fecfin && v.Rodal == rodal select v;
                var noTrozasC = viajes.Sum(c => c.NoTrozasC);
                if (noTrozasC == null) noTrozasC = 0;
                ViewData["NoTrozasC"] = noTrozasC;
                ViewData["VolTrozasC"] = Math.Round((decimal)(noTrozasC * 0.0197683239962652), 2);
                model = result.AsNoTracking().ToList();
            }
            else if(rodal!=null)
            {
                var result = from v in _context.Viajes
                             join s in _context.Trozas on v.ViajeId equals s.ViajeId into joinViajes
                             where v.FechaIngreso >= _fecini && v.FechaIngreso <= _fecfin && v.Rodal==rodal
                             from vi in joinViajes.DefaultIfEmpty()
                             select new ResumenCosecha
                             {
                                 Circunferencia = vi == null ? 0f : vi.Circunferencia,
                                 Clasificacion = vi == null ? "none" : vi.Clasificacion,
                                 Largo = vi == null ? 0f : vi.Largo,
                                 Volumen = vi == null ? 0 : MathF.Round((float)(((vi.Circunferencia * vi.Circunferencia) / (4 * Math.PI)) * vi.Largo / 10000), 2),
                                 NoTrozasC = v.NoTrozasC,
                                 VolTrozasC = 0.0f
                             };
                var viajes = from v in _context.Viajes where v.FechaIngreso >= _fecini && v.FechaIngreso <= _fecfin && v.Rodal == rodal select v;
                var noTrozasC = viajes.Sum(c => c.NoTrozasC);
                if (noTrozasC == null) noTrozasC = 0;
                ViewData["NoTrozasC"] = noTrozasC;
                ViewData["VolTrozasC"] = Math.Round((decimal)(noTrozasC * 0.0197683239962652), 2);
                model = result.AsNoTracking().ToList();
            }

            /*var viajes = from v in _context.Viajes where v.FechaIngreso >= _fecini && v.FechaIngreso <= _fecfin select v;
            var noTrozasC = viajes.Sum(c => c.NoTrozasC);
            if (noTrozasC == null) noTrozasC = 0;
            ViewData["NoTrozasC"] = noTrozasC;
            ViewData["VolTrozasC"] = Math.Round((decimal)(noTrozasC * 0.0197683239962652),2);*/
            var fincas = GetFincas();
            ViewData["Fincas"] = fincas;
            ViewData["FincaSelected"] = finca;
            var rodales = GetRodales();
            ViewData["Rodales"] = rodales;
            var _finca = "";
            if (rodal != null)
            {
                _finca = rodales.Find(m => m.Value == rodal).Text;
            }
            ViewData["RodalSelected"] = rodal;
            ViewData["RodalSelectedText"] = _finca;
            return View(model);
           // return View(await _context.Trozas.AsNoTracking().ToListAsync());
        }

        [Authorize]
        public async Task<IActionResult> ResumenComercial(DateTime fecini, DateTime fecfin)
        {
            List<Estratos> _estratos = new List<Estratos>();
            _estratos.Add(new Estratos { Id = 0, MinValue = 40, MaxValue = 45, Name = "40-44" });
            _estratos.Add(new Estratos { Id = 1, MinValue = 45, MaxValue = 50, Name = "45-49" });
            _estratos.Add(new Estratos { Id = 2, MinValue = 50, MaxValue = 55, Name = "50-54" });
            _estratos.Add(new Estratos { Id = 3, MinValue = 55, MaxValue = 60, Name = "55-59" });
            _estratos.Add(new Estratos { Id = 4, MinValue = 60, MaxValue = 65, Name = "60-64" });
            _estratos.Add(new Estratos { Id = 5, MinValue = 65, MaxValue = 70, Name = "65-69" });
            _estratos.Add(new Estratos { Id = 6, MinValue = 70, MaxValue = 75, Name = "70-74" });
            _estratos.Add(new Estratos { Id = 7, MinValue = 75, MaxValue = 80, Name = "75-79" });
            _estratos.Add(new Estratos { Id = 8, MinValue = 80, MaxValue = 85, Name = "80-84" });
            _estratos.Add(new Estratos { Id = 9, MinValue = 85, MaxValue = 90, Name = "85-89" });
            _estratos.Add(new Estratos { Id = 10, MinValue = 90, MaxValue = 99999, Name = "90->>" });

            ViewData["Estratos"] = _estratos;
            var _fecini = fecini.Year < 1900 ? new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1) : fecini.Date;
            var _fecfin = fecfin.Year < 1900 ? new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month)) : fecfin.Date;
            ViewData["FecIni"] = _fecini.ToString("yyyy-MM-dd");
            ViewData["FecFin"] = _fecfin.ToString("yyyy-MM-dd");
            var result = from s in _context.DetalleContenedores
                         join v in _context.PackingList on s.PackingListId equals v.PackingListId
                         join c in _context.Contenedores on v.PackingListId equals c.PackingListId
                         join f in _context.Facturas on c.FacturaId equals f.FacturaId
                         where f.Fecha >= _fecini && f.Fecha <= _fecfin
                         select new ResumenComercial {
                             Circunferencia = s.Circunferencia,
                             Clasificacion = v.Clase,
                             Longitud = s.Longitud,
                         VolumenS = MathF.Round((float)(((s.Circunferencia * s.Circunferencia) / (4 * Math.PI)) * s.Longitud / 10000), 2),
                         VolumenH = Convert.ToSingle((Math.Pow(((s.Circunferencia / 100.0f) - v.DescuentoCrc), 2) * (s.Longitud - v.DescuentoLong) / 16)),
                         Diametro = Convert.ToSingle(s.Circunferencia/Math.PI)};
            // VolumenH = (float)(Math.Pow(((s.Circunferencia/100.0f)-v.DescuentoCrc),2.0f)*(s.Longitud-v.DescuentoLong)/16.0f)
            return View(await result.AsNoTracking().ToListAsync());
            // return View(await _context.Trozas.AsNoTracking().ToListAsync());
        }

        public async Task<IActionResult> ImportFromExcel(IFormFile theExcel, int Id, int empresaId)
        {
            ExcelPackage excel;
            using (var memoryStream = new MemoryStream())
            {
                await theExcel.CopyToAsync(memoryStream);
                excel = new ExcelPackage(memoryStream);
            }

            //Datos de PackingList
            var workSheet = excel.Workbook.Worksheets[0];
            var start = workSheet.Dimension.Start;
            var end = workSheet.Dimension.End;
            for (int row = start.Row + 1; row <= end.Row; row++)
            {
                if (workSheet.Cells[row, 1].Value != null)
                {

                    PackingList packingList = new PackingList
                    {
                        Consecutivo = Convert.ToInt32(workSheet.Cells[row, 1].Value),
                        Clase = workSheet.Cells[row, 2].Text,
                        Ubicacion = workSheet.Cells[row, 3].Text,
                        Fecha = (DateTime)workSheet.Cells[row, 4].Value,
                        ContenedorId = workSheet.Cells[row, 5].Text,
                        DescuentoCrc = Convert.ToSingle(workSheet.Cells[row, 6].Value),
                        DescuentoLong = Convert.ToSingle(workSheet.Cells[row, 7].Value),
                        PackingListId = workSheet.Cells[row, 8].Text

                    };
                    _context.PackingList.Add(packingList);
                }
            }
            _context.SaveChanges();
        
            //Datos de DetalleContenedores
            var workSheet2 = excel.Workbook.Worksheets[1];
            var start2 = workSheet2.Dimension.Start;
            var end2 = workSheet2.Dimension.End;
            for (int row = start2.Row + 1; row <= end2.Row; row++)
            {
                if (workSheet2.Cells[row, 1].Value != null)
                {
                    DetalleContenedores detalleContenedor = new DetalleContenedores
                    {
                        Circunferencia = Convert.ToSingle(workSheet2.Cells[row, 1].Value),
                        Longitud = Convert.ToSingle(workSheet2.Cells[row, 2].Value),
                        PackingListId = workSheet2.Cells[row, 3].Text
                    };
                    _context.DetalleContenedores.Add(detalleContenedor);
                }
            }
            _context.SaveChanges();

            //Datos de Facturas
            var workSheet3 = excel.Workbook.Worksheets[2];
            var start3 = workSheet3.Dimension.Start;
            var end3 = workSheet3.Dimension.End;
            for (int row = start3.Row + 1; row <= end3.Row; row++)
            {
                if (workSheet3.Cells[row, 1].Value != null)
                {
                    Facturas factura = new Facturas
                    {
                        BillOfLading = workSheet3.Cells[row, 1].Text,
                        Cliente = workSheet3.Cells[row, 2].Text,
                        Destino = workSheet3.Cells[row, 3].Text,
                        Especie = workSheet3.Cells[row, 4].Text,
                        FacturaId = workSheet3.Cells[row, 5].Text,
                        Fecha = (DateTime)workSheet3.Cells[row, 6].Value,
                        Finca = workSheet3.Cells[row, 7].Text,
                        Incoterm = workSheet3.Cells[row, 8].Text

                    };

                   _context.Facturas.Add(factura);
                }
            }
            _context.SaveChanges();

            //Datos de Contenedores
            var workSheet4 = excel.Workbook.Worksheets[3];
            var start4 = workSheet4.Dimension.Start;
            var end4 = workSheet4.Dimension.End;
            for (int row = start4.Row + 1; row <= end4.Row; row++)
            {
                if (workSheet4.Cells[row, 1].Value != null)
                {
                    Contenedores contenedor = new Contenedores
                    {
                        ContenedorId = workSheet4.Cells[row, 1].Text,
                        Sello = workSheet4.Cells[row, 2].Text,
                        Cabezal = workSheet4.Cells[row, 3].Text,
                        Chofer = workSheet4.Cells[row, 4].Text,
                        //                        DescuentoCrc = Convert.ToSingle(workSheet3.Cells[row, 5].Value),
                        //                        DescuentoLong = Convert.ToSingle(workSheet3.Cells[row, 6].Value),
                        FacturaId = workSheet4.Cells[row, 5].Text,
                        FechaSalida = (DateTime)workSheet4.Cells[row, 6].Value,
                        MaxCargoWgt = Convert.ToSingle(workSheet4.Cells[row, 7].Value),
                        PackingListId = workSheet4.Cells[row, 8].Text

                    };
                    _context.Contenedores.Add(contenedor);
                }
            }
            _context.SaveChanges();

            return RedirectToAction(nameof(ResumenComercial));

        }

        [Authorize]
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        private List<SelectListItem> GetRodales()
        {
            var rodales = from s in _context.Rodales orderby s.Rodal select s;
            var rodalesList = new List<SelectListItem>();
            rodalesList.Add(new SelectListItem { Value = "", Text = "" });
            foreach (var val in rodales)
            {
                rodalesList.Add(new SelectListItem { Value = val.Rodal, Text = val.Rodal + " - " + val.Finca });
            }

            return rodalesList;
        }

        private List<SelectListItem> GetFincas()
        {
            var rodales = from s in _context.Rodales orderby s.Rodal select s;
            var fincas = rodales.GroupBy(m => new { m.Nucleo, m.Finca }).Select(s => new {Nucleo = s.Key.Nucleo, Finca = s.Key.Finca}); 
            var fincasList = new List<SelectListItem>();
            fincasList.Add(new SelectListItem { Value = "", Text = "" });
            foreach (var val in fincas)
            {
                fincasList.Add(new SelectListItem { Value = val.Finca, Text = val.Nucleo + " - " + val.Finca });
            }

            return fincasList;
        }
    }
}
